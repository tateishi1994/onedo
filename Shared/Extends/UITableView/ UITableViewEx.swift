//
//  UITableViewEx.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/06.
//  Copyright © 2020 at-works. All rights reserved.
//

import UIKit

public class SectionItem {
    var sectionname : String = ""
    var cellItems : [CellItem] = [CellItem]()
    var bool_open : Bool = false
    var index : Int = 0
}

public class CellItem {
    var title : String = ""
    var id : Int = 0
}

class  UITableViewEx : UITableView {
    //    //セクション名とルーティングループ名のペアの配列
    public var sections = [SectionItem]()

}

extension  UITableViewEx: HeaderViewExDelegate {

    func openedView(section: Int) {

        self.sections[section].bool_open = true
        
        if let numberOfRows = self.dataSource?.tableView(self, numberOfRowsInSection: section) {

            var indexesPathToInsert:[IndexPath] = []

            for i in 0 ..< numberOfRows {
                indexesPathToInsert.append(IndexPath(row: i, section: section))
            }

            if indexesPathToInsert.count > 0 {
                self.beginUpdates()
                self.insertRows(at: indexesPathToInsert, with: UITableView.RowAnimation.automatic)
                self.endUpdates()
            }
        }
    }

    func closedView(section: Int) {

        if let numberOfRows = self.dataSource?.tableView(self, numberOfRowsInSection: section) {
            var indexesPathToDelete:[IndexPath] = []
            self.sections[section].bool_open = false

            for i in 0 ..< numberOfRows {
                indexesPathToDelete.append(IndexPath(row: i, section: section))
            }

            if indexesPathToDelete.count > 0 {
                self.beginUpdates()
                self.deleteRows(at: indexesPathToDelete, with: UITableView.RowAnimation.top)
                self.endUpdates()
            }
        }
    }
}
