//
//  HeaderView.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/06.
//  Copyright © 2020 at-works. All rights reserved.
//

import UIKit

class HeaderViewEx : UIView {
    var tableView :  UITableViewEx!
    var delegate : HeaderViewExDelegate?
    var section = 0

    required init(tableView:  UITableViewEx, section: Int) {
        guard let height = tableView.delegate?.tableView!(tableView, heightForHeaderInSection: section) else {
            fatalError("Please Call heightForHeaderInSection.")
        }
        
        let frame = CGRect(x:0, y:0, width:tableView.frame.width, height:height)
        super.init(frame: frame)
        
        self.tableView = tableView
        self.delegate = tableView
        self.section = section
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Don't Call init(:Coder)")
    }
    
    override func layoutSubviews() {
        let toggleButton = UIButton()
        toggleButton.addTarget(self, action: #selector(self.toggle(sender:)), for: UIControl.Event.touchUpInside)
        toggleButton.backgroundColor = UIColor.clear
        toggleButton.frame = CGRect(x : 0, y : 0, width : self.frame.width, height : self.frame.height)
        self.addSubview(toggleButton)
    }
    
    @objc func toggle(sender : AnyObject){
        if self.tableView?.sections[self.section].bool_open == false {
            delegate?.openedView(section : self.section)
        } else {
            delegate?.closedView(section : self.section)
        }
    }
}


