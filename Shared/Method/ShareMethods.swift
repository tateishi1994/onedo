//
//  ShareMethods.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/01.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation
import RealmSwift

public class ShareMethods {
//== Date関連 =====================================================================================================================
    public class func initDateComponents(from: Date) -> DateComponents {
        return Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second, .nanosecond], from: from)
    }
    
    public class func initDate(_ year:Int, _ month:Int, _ day:Int, _ hour:Int, _ minute:Int, _ second:Int, _ nanosec:Int = 0) -> Date? {
        return DateComponents(calendar: Calendar.current, year: year, month: month, day: day, hour: hour, minute: minute, second: second, nanosecond: nanosec).date
    }
    /**
     *
     * 祝日になる日を判定する
     * (引数) year: Int, month: Int, day: Int, weekdayIndex: Int
     * weekdayIndexはWeekdayのenumに該当する値(0...6)が入る
     * ※1. カレンダーロジックの参考：http://p-ho.net/index.php?page=2s2
     * ※2. 書き方（タプル）の参考：http://blog.kitoko552.com/entry/2015/06/17/213553
     * ※3. [Swift] 関数における引数/戻り値とタプルの関係：http://dev.classmethod.jp/smartphone/swift-function-tupsle/
     *
     */
    public class func isJapaneseAnniversary(date: Date, checkNationalHoliday: Bool = true) -> Bool {
        
        let cal = Calendar(identifier: .gregorian)
        let dComp = ShareMethods.initDateComponents(from: date)
        guard let date = cal.date(from: DateComponents(era: 1,
                                                       year: dComp.year,
                                                       month: dComp.month,
                                                       day: dComp.day,
                                                       hour: 0,
                                                       minute: 0,
                                                       second: 0,
                                                       nanosecond: 0)) else {
                                                        fatalError() // FIXME: throwにしたほうがよい？
        }
        let year = dComp.year!
        let month = dComp.month!
        let day = dComp.day!
        let weekdayNum = cal.component(.weekday, from: date) // 1:日曜日 ～ 7:土曜日
        
        guard let weekday = Weekday(rawValue: weekdayNum - 1) else { fatalError("weekdayIndex is invalid.") }
        
        /// 国民の祝日に関する法律（こくみんのしゅくじつにかんするほうりつ）は、
        /// 1948年（昭和23年）7月20日に公布・即日施行された日本の法律である。通称祝日法。
        /// See also: https://ja.wikipedia.org/wiki/国民の祝日に関する法律
        let PublicHolidaysLawYear = 1948
        
        /// 「国民の祝日」が日曜日に当たるときは、その日後においてその日に最も近い「国民の祝日」でない日を休日とする
        /// 1973年の国民の祝日に関する法律が改正されたことにより制定
        /// See also: https://ja.wikipedia.org/wiki/%E6%8C%AF%E6%9B%BF%E4%BC%91%E6%97%A5
        let AlternateHolidaysLawYear = 1973
        
        /// 「国民の祝日」の特例が2018年（平成30年6月20日）に公布・施行された。
        /// 2020年（平成32年）の東京オリンピック・パラリンピック競技大会の円滑な準備及び運営に資するため
        /// 同年に限り「海の日」は7月23日、「体育の日（スポーツの日）」は7月24日、「山の日」は8月10日となる。
        /// See also: http://www8.cao.go.jp/chosei/shukujitsu/gaiyou.html
        let SpecialProvisionYear = 2020

        /// （注意）春分の日・秋分の日は1948年以前も祝祭日であったが、このカレンダーロジックの基準は1948年〜を基準とするので考慮しない
        /// See also: https://ja.wikipedia.org/wiki/%E7%9A%87%E9%9C%8A%E7%A5%AD

        /// 2019年天皇即位の年
        let emperorthroneYear = 2019

        /// 国民の祝日適用年
        /// 元々、5月上旬における飛石連休の解消・改善を望む当時の世論に応える形で1985年12月27日に祝日法が改正され、
        /// 導入されたものであるが、5月4日に限らず、祝日と祝日に挟まれた平日を全て休日にする制度であることから、
        /// 後の祝日移動に伴い、5月以外の月にも国民の休日が現れることとなった。
        /// See also: https://ja.wikipedia.org/wiki/%E5%9B%BD%E6%B0%91%E3%81%AE%E4%BC%91%E6%97%A5
        let nationalHolidaysStartYear = 1986

        switch (year, month, day, weekday) {
            
            //1月1日: 元旦
            case (_, 1, 1, _):
                return true

            //1月2日: 振替休日
            case (year, 1, 2, .Mon) where year >= AlternateHolidaysLawYear:
                return true
            
            //(1).1月15日(1999年まで)、(2).1月の第2月曜日(2000年から): 成人の日
            case (year, 1, 15, _) where year <= 1999:
                return true
            
            case (year, 1, 8...14, .Mon) where year >= 2000:
                return true
            
            //1月16日: 成人の日の振替休日(1999年まで)
            case (year, 1, 16, .Mon) where year >= AlternateHolidaysLawYear && year <= 1999:
                return true

            //2月11日: 建国記念の日
            case (_, 2, 11, _):
                return true
            
            //2月12日: 振替休日
            case (year, 2, 12, .Mon) where year >= AlternateHolidaysLawYear:
                return true
            
            //2月23日(2020年から): 天皇誕生日
            case (year, 2, 23, _) where year >= 2020:
                return true
            
            //2月24日(2020年から): 天皇誕生日の振替休日
            case (year, 2, 24, .Mon) where year >= 2020:
                return true
            
            //3月20日 or 21日: 春分の日(計算値によって算出)
            case (year, 3, day, _)
                where PublicHolidaysLawYear < year && day == SpringAutumn.spring.calcDay(year: year):
                
                return true

            //春分の日の次が月曜日: 振替休日
            case (year, 3, day, .Mon)
                where year >= AlternateHolidaysLawYear && day == SpringAutumn.spring.calcDay(year: year) + 1:
                
                return true
            
            //4月29日: 1949年から1989年までは天皇誕生日、1990年から2006年まではみどりの日、2007年以降は昭和の日
            case (year, 4, 29, _) where PublicHolidaysLawYear < year:
                return true
            
            //4月30日: 振替休日
            case (year, 4, 30, .Mon) where year >= AlternateHolidaysLawYear:
                return true
            
            //2019年5月1日： 2019年だけ天皇の即位の日
            case (emperorthroneYear, 5, 1, _):
                return true

            //5月3日: 1949年から憲法記念日
            case (year, 5, 3, _) where PublicHolidaysLawYear < year:
                return true

            //5月4日: (1)1988年以前は振替休日、(2).1988年から2006年まで国民の休日、2007年以降はみどりの日
            case (year, 5, 4, _) where PublicHolidaysLawYear < year:
                return true

            /*
            case (year, 5, 4, .Mon) where year < 1988:
                return true

            case (1988...2006, 5, 4, .tue),
                 (1988...2006, 5, 4, .wed),
                 (1988...2006, 5, 4, .thu),
                 (1988...2006, 5, 4, .fri),
                 (1988...2006, 5, 4, .sat):
                
                return true

            case (year, 5, 4, _) where year > 2006:
                return true
            */
            
            //5月5日: 1949年からこどもの日
            case (year, 5, 5, _) where PublicHolidaysLawYear < year:
                return true
            
            //ゴールデンウィークの振替休日
            case (year, 5, 6, _) where year >= AlternateHolidaysLawYear && getGoldenWeekAlterHoliday(year: year, weekday: weekday):
                return true
            
            //(1).7月20日(1996年から2002年まで)、(2).7月の第3月曜日(2003年から)、(3).7月23日(2020年のみ): 海の日
            case (1996...2002, 7, 20, _):
                return true
            
            case (year, 7, 15...21, .Mon)
                where 2003 <= year && year != SpecialProvisionYear:
                return true
            
            case (SpecialProvisionYear, 7, 23, _):
                return true
            
            //7月21日: 海の日の振替休日
            case (1996...2002, 7, 21, .Mon):
                return true
            
            //(1).8月11日(2016年から)、(2).8月10日(2020年のみ): 山の日
            case (year, 8, 11, _)
                where year > 2015 && year != SpecialProvisionYear:
                return true
            
            case (SpecialProvisionYear, 8, 10, _):
                return true
            
            //8月12日: 山の日の振替休日
            case (year, 8, 12, .Mon) where year > 2015:
                return true
            
            //(1).9月15日(1966年から2002年まで)、(2).9月の第3月曜日(2003年から): 敬老の日
            case (1966...2002, 9, 15, _):
                return true
            
            case (year, 9, 15...21, .Mon) where year > 2002:
                return true
            
            //9月16日: 敬老の日の振替休日
            case (1973...2002, 9, 16, .Mon):
                return true
            
            //9月22日 or 23日: 秋分の日(計算値によって算出)
            case (year, 9, day, _)
                where PublicHolidaysLawYear <= year && day == SpringAutumn.autumn.calcDay(year: year):
                
                return true
            
            //秋分の日の次が月曜日: 振替休日
            case (year, 9, day, .Mon)
                where year >= AlternateHolidaysLawYear && day == SpringAutumn.autumn.calcDay(year: year) + 1:
                
                return true
            
            //シルバーウィークの振替休日である(※現行の法律改正から変わらないと仮定した場合2009年から発生する)
            //See also: https://ja.wikipedia.org/wiki/シルバーウィーク
            case (_, 9, _, _)
                where ShareMethods.oldPeopleDay(year: year) < day
                    && day < SpringAutumn.autumn.calcDay(year: year)
                    && getAlterHolidaySliverWeek(year: year) && year > 2008:
                return true
            
            //(1).10月10日(1966年から1999年まで)、(2).10月の第2月曜日(2000年から)、(3).7月24日(2020年のみ): 体育の日(スポーツの日)
            case (1966...1999, 10, 10, _):
                return true
            
            case (year, 10, 8...14, .Mon)
                where year > 1999 && year != SpecialProvisionYear:
                return true
            
            case (SpecialProvisionYear, 7, 24, _):
                return true
            
            //10月11日: 体育の日の振替休日
            case (1966...1999, 10, 11, .Mon):
                return true
            
            //2019年10月22日： 2019年だけ即位礼正殿の儀
            case (emperorthroneYear, 10, 22, _):
                return true

            //11月3日: 1948年から文化の日
            case (year, 11, 3, _) where PublicHolidaysLawYear <= year:
                return true
            
            //11月4日: 振替休日
            case (year, 11, 4, .Mon) where year >= AlternateHolidaysLawYear:
                return true
            
            //11月23日: 1948年から勤労感謝の日
            case (year, 11, 23, _) where PublicHolidaysLawYear <= year:
                return true
            
            //11月24日: 振替休日
            case (year, 11, 24, .Mon) where year >= AlternateHolidaysLawYear:
                return true
            
            //12月23日(1989年から2018年まで): 天皇誕生日
            case (1990...2018, 12, 23, _):
                return true
            
            //12月24日(1989年から2018年まで): 天皇誕生日の振替休日
            case (1990...2018, 12, 24, .Mon):
                return true

            //1999以前の祝日でその年限りに施行された祝日はこちら
            //4月10日: 1959年だけ皇太子明仁親王の結婚の儀
            case (1959, 4, 10, _):
                return true

            //2月24日: 1989年だけ昭和天皇の大喪の礼
            case (1989, 2, 24, _):
                return true

            //11月12日: 1990年だけ即位礼正殿の儀
            case (1990, 11, 12, _):
                return true

            //6月9日: 1993年だけ皇太子徳仁親王の結婚の儀
            case (1993, 6, 9, _):
                return true

            //祝祭日ではない時
            default:
                if checkNationalHoliday && weekdayNum != 1 && nationalHolidaysStartYear <= year {
                    // 前日
                    let beforeDate = cal.date(byAdding: .day, value: -1, to: cal.startOfDay(for: date))
                    // 前日の祝日チェック
                    //再帰呼び出しの場合は国民の休日チェックは行わない
                    let isHolidayBegore = isJapaneseAnniversary(date: beforeDate!,checkNationalHoliday: false)
                    // 翌日
                    let nextDate = cal.date(byAdding: .day, value: 1, to: cal.startOfDay(for: date))
                    // 翌日の祝日チェック
                    //再帰呼び出しの場合は国民の休日チェックは行わない
                    let isHolidayNext = isJapaneseAnniversary(date: nextDate!, checkNationalHoliday: false)
                    //前日と翌日が祝日の平日は国民の休日
                    return  isHolidayBegore && isHolidayNext
                } else {
                    return false
                }
        }
    }
    //指定した日付以降で最も近い指定曜日の日付を取得
    public class func getDate(_ pDate: Date, _ pWeekDay: Weekday) -> Date? {
        var nextDate = pDate
        for _ in 1...7 {
            if Weekday(nextDate) == pWeekDay {
                return nextDate
            }
            nextDate = Calendar.current.date(byAdding: .day, value: 1, to: nextDate)!
        }
        return nil
    }
    //指定月の第XY曜日の日付を取得
    public class func getDate(_ pDate: Date, _ pOrdinalNum: Int, _ pWeekDay: Weekday) -> Date? {
        let component = ShareMethods.initDateComponents(from: pDate)
        return getDate(component.year!, component.month!, pOrdinalNum, pWeekDay)
    }
    //指定月の第XY曜日の日付を取得
    public class func getDate(_ pYear: Int, _ pMonth: Int, _ pOrdinalNum: Int, _ pWeekDay: Weekday) -> Date? {
        let firstDate = ShareMethods.initDate(pYear, pMonth, 1, 0, 0, 0)!
        let firstWeekDay = getDate(firstDate, pWeekDay)!
        var retDate = firstWeekDay
        for i in 1...5 {
            if i == pOrdinalNum {
                let component = ShareMethods.initDateComponents(from: retDate)
                if component.month == pMonth {
                    return retDate
                } else {
                    return nil
                }
            }
            retDate = Calendar.current.date(byAdding: .day, value: 7, to: retDate)!
        }
        return nil
    }
    //Dateを指定されたフォーマットに従って文字列に変換
    public class func convertDateToString(_ pDate: Date, _ pDateFormat: String?) -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        if pDateFormat != nil {
            formatter.dateFormat = pDateFormat!
        }
        return formatter.string(from: pDate)
    }
    //Dateを指定されたフォーマットに従って文字列に変換
    public class func convertDateToString(_ pDate: Date, _ pDateStyle: DateFormatter.Style, _ pTimeStyle: DateFormatter.Style) -> String {
        let formatter: DateFormatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .gregorian)
        formatter.dateStyle = pDateStyle
        formatter.timeStyle = pTimeStyle
        return formatter.string(from: pDate)
    }
    
    //Dateを値が0の場合は単位を表示しないフォーマットで取得
    public class func convertDateToString(_ pDate: Date, _ pFormat: DateTimeFormat) -> String {
        let cmp  = ShareMethods.initDateComponents(from: pDate)
        var str = ""
        if pFormat == DateTimeFormat.OmitZero_All || pFormat == DateTimeFormat.OmitZero_DateOnly {
            if cmp.year != 0 && cmp.year != 1 {
                str += String(cmp.year!) + "年"
            }
            if cmp.month != 0 && cmp.month != 1 {
                str += String(cmp.month!) + "月"
            }
            if cmp.day != 0 && cmp.day != 1 {
                str += String(cmp.day!) + "秒"
            }
        }
        if pFormat == DateTimeFormat.OmitZero_All || pFormat == DateTimeFormat.OmitZero_TimeOnly {
            if cmp.hour != 0 {
                str += String(cmp.hour!) + "時間"
            }
            if cmp.minute != 0 {
                str += String(cmp.minute!) + "分"
            }
            if cmp.second != 0 {
                str += String(cmp.second!) + "秒"
            }
        }
        
        return str
    }
    
//-- private (isJapaneseAnniversary) ------------------------------------------------------------------------------------------------------------------------
    /**
     *
     * ゴールデンウィークの振替休日を判定する
     * 2007年以降で5/6が月・火・水(5/3 or 5/4 or 5/5が日曜日)なら5/6を祝日とする
     * See also: https://www.bengo4.com/other/1146/1288/n_1412/
     *
     */
    private class func getGoldenWeekAlterHoliday(year: Int, weekday: Weekday) -> Bool {
        switch weekday {
        case .Mon where 2007 <= year, .Tue where 2007 <= year, .Wed where 2007 <= year:
            return true
        default:
            return false
        }
    }
    /**
     * シルバーウィークの振替休日を判定する
     * 敬老の日の2日後が秋分の日ならば間に挟まれた期間は国民の休日とする
     */
    private class func getAlterHolidaySliverWeek(year: Int) -> Bool {
        return oldPeopleDay(year: year) + 2 == SpringAutumn.autumn.calcDay(year: year)
    }
    /**
     * 指定した年の敬老の日を調べる
     */
    internal class func oldPeopleDay(year: Int) -> Int {
        let cal = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!

        func dateFromDay(day: Int) -> Date? {
            return cal.date(era: 1, year: year, month: 9, day: 0, hour: 0, minute: 0, second: 0, nanosecond: 0)
        }

        func weekdayAndDayFromDate(date: Date) -> (weekday: Int, day: Int) {
            return (
                weekday: cal.component(.weekday, from: date),
                day:     cal.component(.day, from: date)
            )
        }

        let monday = 2
        return (15...21)
            .map(dateFromDay)
            .compactMap{ $0 }
            .map(weekdayAndDayFromDate)
            .filter{ $0.weekday == monday }
            .first!
            .day
    }

//== ファイル関連 =====================================================================================================================
    public class func loadCSV(filename: String) -> [[String]] {
        
        var csvData = [[String]]()
        let path = Bundle.main.path(forResource:filename, ofType:"csv")
        do {
            let csvText = try String(contentsOfFile: path!, encoding: String.Encoding.utf8)
            let csvLines = csvText.components(separatedBy: .newlines)
            var cnt : Int = 0
            var valueCntInLine : Int = 0
            for csvLine in csvLines {
                let separetedByComma = csvLine.components(separatedBy: ",")
                if cnt == 0 {
                    valueCntInLine = separetedByComma.count
                } else if separetedByComma.count != valueCntInLine || csvLine == "" {
                    continue
                }
                var newRow = [String]()
                for value in separetedByComma {
                    newRow.append(value)
                }
                csvData.append(newRow)
                cnt += 1
            }
        } catch {
            return csvData
        }
        return csvData
    }
//== ダイアログ関連 ===================================================================================================================
    public class func showDialog(_ dialogType: DialogType) {
        switch dialogType {
        case DialogType.NortificationAuthorize:
            // 通知許可ダイアログを表示
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                // エラー処理
            }
        }
        
    }
// == 通知関連 =======================================================================================================================
    public class func showNotification(_ title: String, _ body: String, _ sound: UNNotificationSound? = UNNotificationSound.default) {
        let content = UNMutableNotificationContent()
        // 通知のタイトルを設定
        content.title = title
        // 通知の本文を設定
        content.body = body
        // 通知音を設定
        content.sound = sound
        let request = UNNotificationRequest(identifier: "immediately", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
// == 外部アプリ関連 =======================================================================================================================
    //外部アプリを開く（webサイトのURLを指定した場合はsafariが開きます）
    public class func openApplication(_ pUrl: String) {
        let url = URL(string: pUrl)
//        if( UIApplication.shared.canOpenURL(url!) ) {
        UIApplication.shared.open(url!, options: [:], completionHandler: { result in
            print(result) // → false
        })
//        }
    }
//===================================================================================================================================
}
