//
//  StringMethods.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/07/05.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation

//配列の要素の間に指定した文字列を挟み込んだ文字列を返します。
public class StringMethods {
    public class func Sand(_ pArray: [String], _ pSandStr: String, _ pSandType: SandType) -> String {
        
        if pArray.count == 0 {
            return ""
        }
        var ret: String = ""
        if pSandType == SandType.FULL || pSandType == SandType.LEFT || pSandType == SandType.OUTER {
            ret += pSandStr
        }
        
        for i: Int in 0...pArray.count - 1 {
            let str = pArray[i]
            if pSandType != SandType.OUTER {
                ret += str
                if i != pArray.count - 1 {
                    ret += pSandStr
                }
            }
        }
        if pSandType == SandType.FULL || pSandType == SandType.RIGHT || pSandType == SandType.OUTER {
            ret += pSandStr
        }
        return ret
    }
    
    //文字列が長すぎて入りきらない場合などにはいきれない部分を「...」などで表現した文字列を返す
    public class func Omit(_ pStr: String, _ pMaxLength: Int, pOmitExpression: String = "…") -> String {

        let expLength = pOmitExpression.count
        
        if pStr.count <= pMaxLength || pStr.count < expLength {
            return pStr
        }
        let idxTo = pMaxLength - expLength
        return pStr[..<pStr.index(pStr.startIndex, offsetBy: idxTo)] + pOmitExpression
    }
}


public enum SandType: Int {
    case FULL  //●A●B●C●D●E●
    case RIGHT // A●B●C●D●E●
    case LEFT  //●A●B●C●D●E
    case OUTER //●ABCDE●
    case INNER //A●B●C●D●E
}
