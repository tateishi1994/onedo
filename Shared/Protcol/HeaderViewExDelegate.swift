//
//  HeaderView.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/06.
//  Copyright © 2020 at-works. All rights reserved.
//

import UIKit

protocol HeaderViewExDelegate {
    func openedView(section: Int)
    func closedView(section: Int)
}
