//
//  LoginInfo.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/01.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation

public class LoginInfo {
    var login_userid : String = ""
    var bool_private : Bool = false
}
