//
//  ShareEnum.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/06/11.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation

public enum DBProcess {
    case SELECT
    case INSERT
    case UPDATE
    case UPSERT
    case DELETE
    case DELETE_INSSERT
}

public enum DialogType {
    case NortificationAuthorize
}

public enum Weekday: Int {
    case Sun, Mon, Tue, Wed, Thu, Fri, Sat

    //失敗する可能性のあるコンストラクタを定義（初期値を持った状態にしておく）
    init?(year: Int, month: Int, day: Int) {
        let cal = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!
        let era = 1 //0なら紀元前、1なら紀元後
        guard let date = cal.date(era: era, year: year, month: month, day: day, hour: 0, minute: 0, second: 0, nanosecond: 0) else { return nil }
        let weekdayNum = cal.component(.weekday, from: date)  // 1:日曜日 ～ 7:土曜日
        self.init(rawValue: weekdayNum - 1)
    }
    
    init?(_ pDate: Date) {
        let cal = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!
        let weekdayNum = cal.component(.weekday, from: pDate)  // 1:日曜日 ～ 7:土曜日
        self.init(rawValue: weekdayNum - 1)
    }

    //曜日の日本語表記の定義
    var shortName: String {
        switch self {
        case .Sun: return "日"
        case .Mon: return "月"
        case .Tue: return "火"
        case .Wed: return "水"
        case .Thu: return "木"
        case .Fri: return "金"
        case .Sat: return "土"
        }
    }

    var mediumName: String {
        return shortName + "曜"
    }

    var longName: String {
        return shortName + "曜日"
    }
}

public enum SpringAutumn {
    /// 春分の日
    case spring

    /// 秋分の日
    case autumn

    var constant: Double {
        switch self {
        case .spring: return 20.69115
        case .autumn: return 23.09000
        }
    }

    /// 春分の日・秋分の日を計算する
    /// 参考：http://koyomi8.com/reki_doc/doc_0330.htm
    func calcDay(year: Int) -> Int {
        let x1: Double = Double(year - 2000) * 0.242194
        let x2: Int = Int(Double(year - 2000) / 4)
        return Int(constant + x1 - Double(x2))
    }
}

public enum DateTimeFormat: Int {
    case OmitZero_All = 1
    case OmitZero_DateOnly = 2
    case OmitZero_TimeOnly = 3
}
