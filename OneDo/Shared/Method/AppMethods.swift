//
//  AppMethods.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/06/13.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit
import RealmSwift

public class AppMethods {

//== ▼ 通知関連 ▼ ============================================================================================================================================================================================================================================================
    
    public class func addActiveRoutineNortification(_ realm: Realm, _ loginInfo: LoginInfo) {
        //表示設定
        let routineNow = AppMethods.getRoutine(realm, loginInfo, 0)
        let subRoutineNow = AppMethods.getRoutine(realm, loginInfo, 0, true)
//        let routineNext = AppMethods.getRoutine(realm, loginInfo, RoutineType.NEXT)
//        let user = realm.objects(mm_m_user.self).filter("mailaddress = %@", loginInfo.login_userid).first!
        
        if routineNow == nil {
            addSelectEventNortification(realm, loginInfo)
            return
        }

        let content = UNMutableNotificationContent()
        let subRoutineid: Int
        if subRoutineNow == nil {
            subRoutineid = 0
        } else {
            subRoutineid = subRoutineNow!.routineid
        }
        content.userInfo = ["routineid": routineNow!.routineid, "routineid_sub": subRoutineid, "mailaddress": loginInfo.login_userid]
        content.title = "🆕" + routineNow!.routinename
        if routineNow!.subtitle != "" {
            content.subtitle = "💬" + routineNow!.subtitle
        }
        var bodyText: String = ""
        
//        bodyText += "⏳" + ShareMethods.convertDateToString(routineNow!.requiredtime, DateFormatter.Style.none, DateFormatter.Style.medium)
//        bodyText += "⏳" + ShareMethods.convertDateToString(routineNow!.requiredtime, DateTimeFormat.OmitZero_TimeOnly)
        if subRoutineNow != nil {
            if subRoutineid != routineNow!.routineid {
                bodyText += "\n" + StringMethods.Omit(String(format: "🎵%@", subRoutineNow!.routinename), AppValue.ViewSetting.NORTIFICATION_LINE_LENGTH_IPHONE7)
            } else {
                //メインルーティンとサブルーティンが同じ場合
                //次に優先度の高いサブルーティンを表示
//                for i in 2 ... realm.objects(od_t_routine.self).count - 2 {
//                    let subRoutine = AppMethods.getRoutine(realm, loginInfo, i, true)
//                    if subRoutine != nil  {
//                        bodyText += "\n" + StringMethods.Omit(String(format: "🎵%@", subRoutine!.routinename), AppValue.ViewSetting.NORTIFICATION_LINE_LENGTH_IPHONE7)
//                        break
//                    }
//                }
            }
        }
        let tasks = realm.objects(od_t_task.self).filter("routineid = %@", routineNow!.routineid).sorted(byKeyPath: "int_priority", ascending: true)
        if tasks.count > 0 {
            if bodyText != "" {
                bodyText += "\n"
            }
            var arrayTask = [String]()
            for task in tasks {
                let lineText = StringMethods.Omit("☑️" + task.taskname, AppValue.ViewSetting.NORTIFICATION_LINE_LENGTH_IPHONE7) //!!!!! 機種に合わせて動的にセットされるように修正する必要がある
                arrayTask.append(lineText)
            }
            bodyText += StringMethods.Sand(arrayTask, "\n", SandType.INNER)
        }
        let routine_important = AppMethods.getRoutine(realm, loginInfo, false, true).filter("bool_important = true")
        if bodyText != "" {
            bodyText += "\n"
        }
        var arrayImportantRoutine = [String]()
        var cnt = 0
        for row in routine_important {
            if cnt == 5 {
                break
            }
            if routineNow!.routineid == row.routineid {
                continue
            }
            arrayImportantRoutine.append("⭐️" + row.routinename)
            cnt += 1
        }
        
        
        bodyText += StringMethods.Sand(arrayImportantRoutine, "\n", SandType.INNER)
//        if routineNext != nil {
//            bodyText += "\n" + String(format: "👉%@", routineNext!.routinename)
//        }
        
        
        if bodyText != "" {
            content.body = bodyText
        }
//        content.sound = UNNotificationSound.default
        content.sound = UNNotificationSound.init(named: UNNotificationSoundName(rawValue: "1up.mp3"))
        content.categoryIdentifier = "routine_action"
        
        if let path = Bundle.main.path(forResource: "OneUp_Title", ofType: "png") {
            content.attachments = [try! UNNotificationAttachment(identifier: "routine_action_image", url: URL(fileURLWithPath: path), options: [UNNotificationAttachmentOptionsThumbnailClippingRectKey : CGRect(x: 30, y: 0, width: 5, height: 1).dictionaryRepresentation])]
        }
        
        //アクション設定
        var actions = [UNNotificationAction]()
//        let actionStart = UNNotificationAction(identifier: ActionIdentifierRoutine.START.rawValue, title: ActionIdentifierRoutine.START.name, options: [.foreground])
        var actionURL: UNNotificationAction? = nil
        if routineNow!.routine_url != "" {
            //ルーティンにURLが設定されている場合
            actionURL = UNNotificationAction(identifier: ActionIdentifierRoutine.URL.rawValue, title: ActionIdentifierRoutine.URL.name)
        }
        let actionFinish = UNNotificationAction(identifier: ActionIdentifierRoutine.FINISH.rawValue, title: ActionIdentifierRoutine.FINISH.name)
        let actionHold = UNTextInputNotificationAction(identifier: ActionIdentifierRoutine.HOLD.rawValue, title: ActionIdentifierRoutine.HOLD.name)
        let actionFinishSubRoutine = UNNotificationAction(identifier: ActionIdentifierRoutine.FINISH_SUB.rawValue, title: ActionIdentifierRoutine.FINISH_SUB.name)
        let actionHoldSubRoutine = UNTextInputNotificationAction(identifier: ActionIdentifierRoutine.HOLD_SUB.rawValue, title: ActionIdentifierRoutine.HOLD_SUB.name)
        let actionSelectEvent = UNNotificationAction(identifier: ActionIdentifierRoutine.SELECT_EVENT.rawValue, title: ActionIdentifierRoutine.SELECT_EVENT.name)
        if actionURL != nil {
            actions.append(actionURL!)
        }
        actions.append(actionFinish)
        actions.append(actionHold)
        actions.append(actionFinishSubRoutine)
        actions.append(actionHoldSubRoutine)
        actions.append(actionSelectEvent)
        
        let category = UNNotificationCategory(identifier: "routine_action", actions: actions, intentIdentifiers: [], options: [])
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests() // トリガーされている全ての通知をトリガー解除する
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in if granted { print("通知許可")}} // 「通知を許可しますか？」ダイアログを出す
        center.setNotificationCategories([category])
        
//        タイミング設定
                    let trigger1 = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                    //通知設定
                    let request1 = UNNotificationRequest(identifier: "routine_action_1", content: content, trigger: trigger1)

                    center.add(request1)

        let trigger2 = UNTimeIntervalNotificationTrigger(timeInterval: 60, repeats: true)
        //通知設定
        let request2 = UNNotificationRequest(identifier: "routine_action_2", content: content, trigger: trigger2)

        center.add(request2)
        
//                for i : Double in stride(from: 0, to: 60, by: 1) {
//                    var second = i * 60
//                    let identifier = "routine_action" + String(i)
//                    if i == 0 {
//                        second = 1
//                    }
//                    //タイミング設定
//                    let trig = UNTimeIntervalNotificationTrigger(timeInterval: second, repeats: false)
//                    //通知設定
//                    let req = UNNotificationRequest(identifier: identifier , content: content, trigger: trig)
//
//                    center.add(req)
//                }

        
        let app = UIApplication.shared.delegate as! AppDelegate
        app.addNortification(center)
        
    }
    
    public class func addSelectEventNortification(_ realm: Realm, _ loginInfo: LoginInfo) {
        let content = UNMutableNotificationContent()
        content.userInfo = ["mailaddress": loginInfo.login_userid]
        content.title = "イベントを選択してください。"
        content.sound = UNNotificationSound.default
        content.categoryIdentifier = "action_select_event"
        //アクション設定
        var actions = [UNNotificationAction]()
        for event in realm.objects(mm_m_event.self).filter("bool_nortification_action == true") {
            let action = UNNotificationAction(identifier: String(event.eventid), title: event.eventname)
            actions.append(action)
            var str_on = ""
            if event.bool_event_on == true {
                str_on = "💡"
            }
            content.body = content.body + event.eventname + str_on + "\n"
        }
        let category = UNNotificationCategory(identifier: "action_select_event", actions: actions, intentIdentifiers: [], options: [])
        //タイミング設定
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        //通知設定
        let request = UNNotificationRequest(identifier: "action_select_event", content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests() // トリガーされている全ての通知をトリガー解除する
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in if granted { print("通知許可")}} // 「通知を許可しますか？」ダイアログを出す
        center.setNotificationCategories([category])
        center.add(request)
                
        let app = UIApplication.shared.delegate as! AppDelegate
        app.addNortification(center)
    }
    
//== ▼ ルーティン関連 ▼ ============================================================================================================================================================================================================================================================
    
    public class func getRoutine(_ pRealm: Realm, _ userInfo: LoginInfo, _ pIndex: Int = 0, _ pFlgSubRoutine: Bool = false) -> od_t_routine? {
        
        let routines = getRoutine(pRealm, userInfo, pFlgSubRoutine)
        
        var routineNow: od_t_routine?
        
        if routines.count >= pIndex + 1 {
            routineNow = routines.count == 0 ? nil : routines[pIndex]
        } else {
            return nil
        }
        return routineNow
    }
    public class func carryForwardRoutine(_ pRealm: Realm, _ pRoutine: od_t_routine, _ pProgramName: String, _ pLoginInfo: LoginInfo) {
        //翌日に繰越
        try! pRealm.write {
            let cmp_start = ShareMethods.initDateComponents(from: pRoutine.temp_startdatetime)
            let cmp_end = ShareMethods.initDateComponents(from: pRoutine.enddatetime)
            pRoutine.temp_startdatetime = ShareMethods.initDate(cmp_start.year!, cmp_start.month!, cmp_start.day! + 1, cmp_start.hour!, cmp_start.minute!, cmp_start.second!)!
            pRoutine.enddatetime = ShareMethods.initDate(cmp_end.year!, cmp_end.month!, cmp_end.day! + 1, cmp_end.hour!, cmp_end.minute!, cmp_end.second!)!
            RealmMethods.save(pRealm, pRoutine, DBProcess.UPDATE, pProgramName, pLoginInfo) //DB更新
        }
    }
    public class func finishRoutine(_ pRealm: Realm, _ pRoutine: od_t_routine, _ pProgramName: String, _ pLoginInfo: LoginInfo) {
        
        try! pRealm.write {
            
            let span = AppValue.DB.Table.mm_m_value.span.self
            let cmp_start = ShareMethods.initDateComponents(from: pRoutine.startdatetime)
            let cmp_now = ShareMethods.initDateComponents(from: Date())
            let date_start = ShareMethods.initDate(cmp_now.year!, cmp_now.month!, cmp_now.day!, cmp_start.hour!, cmp_start.minute!, cmp_start.second!)
            switch pRoutine.spanid {
            case span.Year:
                pRoutine.startdatetime = Calendar.current.date(byAdding: .year, value: pRoutine.int_span, to: pRoutine.startdatetime)!
                pRoutine.enddatetime = Calendar.current.date(byAdding: .year, value: pRoutine.int_span, to: pRoutine.enddatetime)!
                pRoutine.temp_startdatetime = pRoutine.startdatetime
            case span.Month:
                pRoutine.startdatetime = Calendar.current.date(byAdding: .month, value: pRoutine.int_span, to: pRoutine.startdatetime)!
                pRoutine.enddatetime = Calendar.current.date(byAdding: .month, value: pRoutine.int_span, to: pRoutine.enddatetime)!
                pRoutine.temp_startdatetime = pRoutine.startdatetime
            case span.Week:
                pRoutine.startdatetime = Calendar.current.date(byAdding: .day, value: pRoutine.int_span * 7, to: date_start!)!
                pRoutine.enddatetime = Calendar.current.date(byAdding: .day, value: pRoutine.int_span * 7, to: pRoutine.enddatetime)!
                pRoutine.temp_startdatetime = pRoutine.startdatetime
            case span.Day:
                pRoutine.startdatetime = Calendar.current.date(byAdding: .day, value: pRoutine.int_span, to: date_start!)!
                pRoutine.enddatetime = Calendar.current.date(byAdding: .day, value: pRoutine.int_span, to: pRoutine.enddatetime)!
                pRoutine.temp_startdatetime = pRoutine.startdatetime
            case span.Hour:
                AppMethods.finishRoutineTimeSpan(pRealm, pRoutine, Calendar.Component.hour)
            case span.Minute:
                AppMethods.finishRoutineTimeSpan(pRealm, pRoutine, Calendar.Component.minute)
            case span.Second:
                AppMethods.finishRoutineTimeSpan(pRealm, pRoutine, Calendar.Component.second)
            case span.Sun0, span.Mon0, span.Tue0, span.Wed0, span.Thu0, span.Fri0, span.Sat0:
                //毎週●曜日
                let value = pRealm.objects(mm_m_value.self).filter("category == %@ && key == %@", "span", pRoutine.spanid).first!
                let nextWeekDay = ShareMethods.getDate(Calendar.current.date(byAdding: .day, value: 1, to: pRoutine.startdatetime)!, Weekday(rawValue: value.int1)!)
                let cmpNextDate = ShareMethods.initDateComponents(from: nextWeekDay!)
                let cmpStartDate = ShareMethods.initDateComponents(from: pRoutine.startdatetime)
                let cmpEndDate = ShareMethods.initDateComponents(from: pRoutine.enddatetime)
                pRoutine.startdatetime = ShareMethods.initDate(cmpNextDate.year!, cmpNextDate.month!, cmpNextDate.day!, cmpStartDate.hour!, cmpStartDate.minute!, cmpStartDate.second!)!
                pRoutine.enddatetime = ShareMethods.initDate(cmpNextDate.year!, cmpNextDate.month!, cmpNextDate.day!, cmpEndDate.hour!, cmpEndDate.minute!, cmpEndDate.second!)!
                pRoutine.temp_startdatetime = pRoutine.startdatetime
            default:
                //第XY曜日指定
                let value = pRealm.objects(mm_m_value.self).filter("category == %@ && key == %@", "span", pRoutine.spanid).first!
                var nextDate: Date?
                for span in stride(from: pRoutine.int_span, to: 36, by: pRoutine.int_span) {
                    //第５●曜日に設定されているとnilになる場合があるため何らかの値が入るまでループ
                    let nextDateMonth = Calendar.current.date(byAdding: .month, value: span, to: pRoutine.startdatetime)!
                    let component = ShareMethods.initDateComponents(from: nextDateMonth)
                    nextDate = ShareMethods.getDate(component.year!, component.month!, value.int2, Weekday(rawValue: value.int1)!)
                    if nextDate != nil {
                        break
                    }
                }
                let cmpNextDate = ShareMethods.initDateComponents(from: nextDate!)
                let cmpStartDate = ShareMethods.initDateComponents(from: pRoutine.startdatetime)
                let cmpEndDate = ShareMethods.initDateComponents(from: pRoutine.enddatetime)
                pRoutine.startdatetime = ShareMethods.initDate(cmpNextDate.year!, cmpNextDate.month!, cmpNextDate.day!, cmpStartDate.hour!, cmpStartDate.minute!, cmpStartDate.second!)!
                pRoutine.enddatetime = ShareMethods.initDate(cmpNextDate.year!, cmpNextDate.month!, cmpNextDate.day!, cmpEndDate.hour!, cmpEndDate.minute!, cmpEndDate.second!)!
                pRoutine.temp_startdatetime = pRoutine.startdatetime
            }
            
            RealmMethods.save(pRealm, pRoutine, DBProcess.UPDATE, pProgramName, pLoginInfo) //DB更新
        }
    }
    public class func holdRoutine(_ pRealm :Realm, _ pRoutine: od_t_routine, _ pHoldMinutes: Int, _ pProgramname: String, _ pLoginInfo: LoginInfo) {
        
        let holdStartDate = Calendar.current.date(byAdding: .minute, value: pHoldMinutes, to: Date())!
        if holdStartDate.compare(pRoutine.enddatetime) == .orderedDescending {
            //指定された分数だけ保留すると締切時刻を過ぎてしまう場合
            carryForwardRoutine(pRealm, pRoutine, pProgramname, pLoginInfo)
        } else {
            try! pRealm.write {
                //保留しても締切日時を超えない場合
                pRoutine.temp_startdatetime = holdStartDate
                RealmMethods.save(pRealm, pRoutine, DBProcess.UPDATE, pProgramname, pLoginInfo) //開始日時を更新
            }
        }
    }
    public class func modifyRoutine(_ pRealm :Realm, _ programname: String, _ loginInfo: LoginInfo) {
        
        let routine = pRealm.objects(od_t_routine.self)
        
        for row in routine {
            let now = Date()
            if row.enddatetime.compare(now) == .orderedAscending {
                //現在日時が終了日時を超えている場合
                carryForwardRoutine(pRealm, row, programname, loginInfo)
            }
        }
    }
    
//-- private (getRoutine) -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    private class func getRoutine(_ pRealm: Realm, _ userInfo: LoginInfo, _ pFlgSubRoutine: Bool = false, _ pFlgToday: Bool = false) -> Results<od_t_routine> {
        
        let routine_setting = AppValue.DB.Table.mm_m_value.routine_setting.self
        let user = pRealm.objects(mm_m_user.self).filter("mailaddress == %@", userInfo.login_userid).first!
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormatter.dateFormat(fromTemplate: "E", options: 0, locale: Locale.current)
        let dayOfWeek = formatter.string(from: now)
        var dayOfWeekDict = Dictionary<String, String>()
        dayOfWeekDict.updateValue("bool_monday == true", forKey: "Mon")
        dayOfWeekDict.updateValue("bool_tuesday == true", forKey: "Tue")
        dayOfWeekDict.updateValue("bool_wednesday == true", forKey: "Wed")
        dayOfWeekDict.updateValue("bool_thursday == true", forKey: "Thu")
        dayOfWeekDict.updateValue("bool_friday == true", forKey: "Fri")
        dayOfWeekDict.updateValue("bool_saturday == true", forKey: "Sat")
        dayOfWeekDict.updateValue("bool_sunday == true", forKey: "Sun")
        
        let flgTodayIsUserHoliday = AppMethods.isUserHoliday(pRealm, user, Date())
        
        let weather = user.category_weather
        var paramWeather1 = ""
        var paramWeather2 = ""
        var paramWeather3 = ""
        switch weather {
        case AppValue.DB.Table.mm_m_value.weather.sunny:
            paramWeather1 = routine_setting.weather_sunny_only
            paramWeather2 = routine_setting.weather_rainy_not
            paramWeather3 = routine_setting.weather_cloudy_not
        case AppValue.DB.Table.mm_m_value.weather.rainy:
            paramWeather1 = routine_setting.weather_rainy_only
            paramWeather2 = routine_setting.weather_sunny_not
            paramWeather3 = routine_setting.weather_cloudy_not
        case AppValue.DB.Table.mm_m_value.weather.cloudy:
            paramWeather1 = routine_setting.weather_cloudy_only
            paramWeather2 = routine_setting.weather_sunny_not
            paramWeather3 = routine_setting.weather_rainy_not
        default:
            paramWeather1 = ""
            paramWeather2 = ""
            paramWeather3 = ""
        }
        let weekdaykb = flgTodayIsUserHoliday == true ? routine_setting.weekday_holiday_only : routine_setting.weekday_normal_only
        
        var routines = pRealm.objects(od_t_routine.self)
            .filter("temp_startdatetime <= %@ && enddatetime >= %@", now, now)
            .filter(dayOfWeekDict[dayOfWeek]!)
            .filter("category_weekday == %@ || category_weekday == %@", routine_setting.nothing, weekdaykb)
            .sorted(byKeyPath: "double_priority", ascending: false)
        
        if pFlgToday == false {
            routines = routines.filter("category_weather == %@ || category_weather == %@ || category_weather == %@", routine_setting.nothing, paramWeather1, paramWeather2, paramWeather3)
            let event_on = pRealm.objects(mm_m_event.self).filter("bool_event_on == true").first!
            for routine: od_t_routine in routines {
                let routine_event = pRealm.objects(od_t_routine_event.self).filter("routineid == %@ && eventid == %@", routine.routineid, event_on.eventid).first!
                if routine_event.bool_effective == false {
                    routines = routines.filter("routineid != %@", routine.routineid)
                }
            }
        }
        
        if userInfo.bool_private == false {
            //◇ログインユーザーのPrivateモードがOFFの時
            //▼Privateモードの時のみ表示するルーティンは取得しない
            routines = routines.filter(" bool_private == false ")
        }
        
        if pFlgSubRoutine == true {
            routines = routines.filter("bool_simultaneous == true ")
        }
        
        return routines
    }
    private class func isUserHoliday(_ pRealm: Realm, _ pUser: mm_m_user, _ pDate: Date) -> Bool {
        
        let todayYoubi: Weekday = Weekday(pDate)!
        let user_holiday = pRealm.objects(mm_m_user_holiday.self).filter("userid == %@", pUser.userid)
        
        for row in user_holiday {
            let holiday = pRealm.objects(mm_m_value.self).filter("id == %@", row.holidayid).first!
            let holidayYoubi = holiday.int1
            if todayYoubi.rawValue == holidayYoubi {
                //今日の曜日がユーザーが設定した休日の曜日と一致する場合
                return true
            }
            if holiday.cd == AppValue.DB.Table.mm_m_value.holiday.Anniversary {
                //ユーザーが祝日を休日として設定している場合
                return ShareMethods.isJapaneseAnniversary(date: pDate, checkNationalHoliday: true)
            }
        }
        return false
    }
    private class func finishRoutineTimeSpan(_ pRealm: Realm, _ pRoutine: od_t_routine, _ pComp: Calendar.Component) {
        let next_start = Calendar.current.date(byAdding: pComp, value: pRoutine.int_span, to: Date())!
        if next_start.compare(pRoutine.enddatetime) == .orderedDescending {
            //次回の実行日時が締め切り日時を超える場合は翌日にする
            pRoutine.startdatetime = Calendar.current.date(byAdding: .day, value: 1, to: pRoutine.startdatetime)!
            pRoutine.enddatetime = Calendar.current.date(byAdding: .day, value: 1, to: pRoutine.enddatetime)!
            pRoutine.temp_startdatetime = pRoutine.startdatetime
        } else {
            pRoutine.temp_startdatetime = next_start
        }
    }
    
//== ▼ イベント関連 ▼ ============================================================================================================================================================================================================================================================
    
    public class func switchEventState(_ pRealm: Realm, _ pEventid: Int, _ pProgramName: String, _ pLoginInfo: LoginInfo) {
        try! pRealm.write {
            let events = pRealm.objects(mm_m_event.self)
            for event in events {
                if event.eventid == pEventid {
                    event.bool_event_on = true
                } else {
                    event.bool_event_on = false
                }
                RealmMethods.save(pRealm, event, DBProcess.UPDATE, pProgramName, pLoginInfo)
            }
        }
    }
    
//== ▼ ユーザーイベント関連 ▼ ============================================================================================================================================================================================================================================================
    
    public class func shiftBreakTime(_ pRealm: Realm, _ pUser: mm_m_user, _ pFlgStart: Bool, _ pProgramName: String, _ pLoginInfo: LoginInfo) {
        try! pRealm.write {
            pUser.bool_breaktime = pFlgStart
            RealmMethods.save(pRealm, pUser, DBProcess.UPDATE, pProgramName, pLoginInfo) //DB更新
        }
    }
    public class func shift_HomeOut(_ pRealm: Realm, _ pUser: mm_m_user, _ pFlgOutSide: Bool, _ pProgramName: String, _ pLoginInfo: LoginInfo) {
        try! pRealm.write {
            if pFlgOutSide == true {
                pUser.category_place = AppValue.DB.Table.mm_m_value.place.outside
            } else {
                pUser.category_place = AppValue.DB.Table.mm_m_value.place.home
            }
            RealmMethods.save(pRealm, pUser, DBProcess.UPDATE, pProgramName, pLoginInfo) //DB更新
        }
    }
    
//== ▼ テストデータ関連 ▼ ============================================================================================================================================================================================================================================================
    
    public class func upsertTestData(_ pRealm :Realm, _ programname: String, _ loginInfo: LoginInfo) {
            
        try! pRealm.write {
            let d1 = pRealm.objects(od_t_routine_event.self)
            for row in d1 {
                pRealm.delete(row)
            }
            let d = pRealm.objects(od_t_task.self)
            for row in d {
                pRealm.delete(row)
            }
            
//            RealmMethods.importCSV(pRealm, "mm_m_value", mm_m_value(), programname, loginInfo, setValue) //バリューマスタをインポート
//            RealmMethods.importCSV(pRealm, "mm_m_user", mm_m_user(), programname, loginInfo, setValue) //ユーザーマスタをインポート
//            RealmMethods.importCSV(pRealm, "mm_m_user_holiday", mm_m_user_holiday(), programname, loginInfo, setValue) //ユーザー休日マスタをインポート
//            RealmMethods.importCSV(pRealm, "mm_m_event", mm_m_event(), programname, loginInfo, setValue) //ユーザー休日マスタをインポート
//            RealmMethods.importCSV(pRealm, "od_t_flow", od_t_flow(), programname, loginInfo, setValue) //ルーティンフローをインポート
//            RealmMethods.importCSV(pRealm, "od_t_folder", od_t_folder(), programname, loginInfo, setValue) //ルーティンフォルダをインポート
//            RealmMethods.importCSV(pRealm, "od_t_routinegrp", od_t_routinegrp(), programname, loginInfo, setValue) //ルーティングループをインポート
            RealmMethods.importCSV(pRealm, "od_t_routine", od_t_routine(), programname, loginInfo, setValue) //ルーティンをインポート
            RealmMethods.importCSV(pRealm, "od_t_task", od_t_task(), programname, loginInfo, setValue) //ルーティンタスクをインポート
                
            let routine = pRealm.objects(od_t_routine.self) //ルーティンデータを取得
            //登録したルーティンデータのstartdatetimeとenddatetimeの日付を現在日付でUPDATE
            for row in routine {

                let startdatetime = row.startdatetime
                let enddatetime = row.enddatetime
                let temp_startdatetime = row.temp_startdatetime

                var cmp_now = ShareMethods.initDateComponents(from: Date())
                let cmp_start = ShareMethods.initDateComponents(from: startdatetime)
                let cmp_end = ShareMethods.initDateComponents(from: enddatetime)
                let cmp_temp_start = ShareMethods.initDateComponents(from: temp_startdatetime)
                var cmp_end_day = cmp_now
                
                var weekday: Weekday? = nil
                
                switch row.spanid {
                case AppValue.DB.Table.mm_m_value.span.Mon0: weekday = Weekday.Mon
                case AppValue.DB.Table.mm_m_value.span.Tue0: weekday = Weekday.Tue
                case AppValue.DB.Table.mm_m_value.span.Wed0: weekday = Weekday.Wed
                case AppValue.DB.Table.mm_m_value.span.Thu0: weekday = Weekday.Thu
                case AppValue.DB.Table.mm_m_value.span.Fri0: weekday = Weekday.Fri
                case AppValue.DB.Table.mm_m_value.span.Sat0: weekday = Weekday.Sat
                case AppValue.DB.Table.mm_m_value.span.Sun0: weekday = Weekday.Sun
                default: weekday = nil
                }
                var flgWeekday = false
                if weekday != nil {
                    let nextWeekday = ShareMethods.getDate(Date(), weekday!)
                    let cmp_nextWeekday = ShareMethods.initDateComponents(from: nextWeekday!)
                    cmp_now.year = cmp_nextWeekday.year
                    cmp_now.month = cmp_nextWeekday.month
                    cmp_now.day = cmp_nextWeekday.day
                    flgWeekday = true
                }
                
                if (cmp_end.hour == 0 && cmp_end.minute == 0 && cmp_end.second == 0) || (cmp_end.hour == 3 && cmp_end.minute == 0 && cmp_end.second == 0){
                    cmp_end_day.day = cmp_now.day! + 1
                }

                if cmp_start.year! == 1 || flgWeekday == true {
                    let new_start = ShareMethods.initDate(cmp_now.year!, cmp_now.month!, cmp_now.day!, cmp_start.hour!, cmp_start.minute!, cmp_start.second!)
                    let new_end = ShareMethods.initDate(cmp_now.year!, cmp_now.month!, cmp_end_day.day!, cmp_end.hour!, cmp_end.minute!, cmp_end.second!)
                    row.startdatetime = new_start!
                    row.temp_startdatetime = new_start!
                    row.enddatetime = new_end!
                    RealmMethods.save(pRealm, row, DBProcess.UPDATE, programname, loginInfo)
                }
                
                if cmp_temp_start.year! == 1 {
                    row.temp_startdatetime = row.startdatetime
                    RealmMethods.save(pRealm, row, DBProcess.UPDATE, programname, loginInfo)
                }
                
            }
        }
    }
    public class func updateTestData(_ pRealm :Realm, _ filename: String, _ programname: String, _ loginInfo: LoginInfo) {
        //idが一致すれば値を更新する
    }
    
//-- private (upsertTestData) ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    private class func setValue<T: RealmObjectEx>(_ pRealm: Realm, _ pRealmObject: T, _ columnName: String, _ columnType: String, _ value: String) {
    
    if value == "" && columnName != "temp_startdatetime" {
        return
    }
    
    switch columnType {
    case "String":
        pRealmObject.setValue(value, forKey: columnName)
    case "Int":
          pRealmObject.setValue(Int(value), forKey: columnName)
        
    case "Double":
        pRealmObject.setValue(Double(value), forKey: columnName)
    case "Bool", "Boolean":
        var boolVal: Bool = false
        if value.lowercased() == "true" {
            boolVal = true
        } else if value.lowercased() == "false" {
            boolVal = false
        }
        if pRealmObject.self is od_t_routine {
            let routine_event = od_t_routine_event()
            switch columnName {
            case "breaktime_public":
                routine_event.eventid = 1
            case "breaktime_private":
                routine_event.eventid = 2
            case "home":
                routine_event.eventid = 3
            case "outside_before":
                routine_event.eventid = 4
            case "outside":
                routine_event.eventid = 5
            case "home_before":
                routine_event.eventid = 6
            case "move_walk":
                routine_event.eventid = 7
            case "move_car":
                routine_event.eventid = 8
            case "move_public":
                routine_event.eventid = 9
            case "study":
                routine_event.eventid = 10
            case "workplace":
                routine_event.eventid = 11
            case "breaktime_evening":
                routine_event.eventid = 12
            case "realhome_before":
                routine_event.eventid = 13
            case "realhome":
                routine_event.eventid = 14
            case "realhome_after":
                routine_event.eventid = 15
            default:
                pRealmObject.setValue(boolVal, forKey: columnName)
            }
            
            switch columnName {
            case "home", "realhome", "outside", "home_before", "workplace", "breaktime_public", "breaktime_private", "outside_before",
                 "move_walk", "move_car", "move_public", "study", "breaktime_evening", "realhome_before", "realhome_after":
                routine_event.routineid = pRealmObject.value(forKey: od_t_routine.primaryKey()!) as! Int
                routine_event.bool_effective = boolVal
                RealmMethods.save(pRealm, routine_event, DBProcess.UPSERT, "MainViewController", LoginInfo())
            default:
                return
            }
        } else {
            pRealmObject.setValue(boolVal, forKey: columnName)
        }
    case "Date":
        //DateFormatterがうまくいかなかった
        let separatedBySpace = value.components(separatedBy: " ")
        var separatedBySlash : [String]
        var separatedByColon : [String]
        
        if separatedBySpace.count == 2 {
            let datePart = separatedBySpace[0]
            let timePart = separatedBySpace[1]
            separatedBySlash = datePart.components(separatedBy: "/")
            separatedByColon = timePart.components(separatedBy: ":")
        } else {
            separatedByColon = value.components(separatedBy: ":")
            separatedBySlash = value.components(separatedBy: "/")
        }
        
        var year : Int = 0
        var month : Int = 0
        var day : Int = 0
        var hour : Int = 0
        var min : Int = 0
        var sec : Int = 0
        var nanosec : Int = 0
        
        if separatedBySlash.count == 3 {
            year = Int(separatedBySlash[0])!
            month = Int(separatedBySlash[1])!
            day = Int(separatedBySlash[2])!
        } else {
            year = 1
            month = 1
            day = 1
        }
        
        if separatedByColon.count == 3 {
                hour = Int(separatedByColon[0])!
                min = Int(separatedByColon[1])!
                let separatedByPeriod = separatedByColon[2].components(separatedBy: ".")
            if separatedByPeriod.count == 2 {
                sec = Int(separatedByPeriod[0])!
                nanosec = Int(separatedByPeriod[1])!
            } else {
                sec = Int(separatedByColon[2])!
                nanosec = 0
            }
        }
        
        if pRealmObject.self is od_t_routine {
            let keyColumnName = od_t_routine.primaryKey()!
            let key = pRealmObject.value(forKey: keyColumnName)!
            let routine = pRealm.objects(od_t_routine.self).filter(keyColumnName + " == %@", key)
            if routine.count > 0 {
                if columnName == "startdatetime" {
                    let startdatetime = routine[0].temp_startdatetime
                    let csvDate = ShareMethods.initDate(year, month, day, hour, min, sec)!
                    if startdatetime.compare(csvDate) == .orderedDescending {
                        //DBデータの日付がCSVデータより後の場合
                        let cmpStart = ShareMethods.initDateComponents(from: startdatetime)
                        year = cmpStart.year!
                        month = cmpStart.month!
                        day = cmpStart.day!
                    }
                } else if columnName == "enddatetime" {
                    let enddatetime = routine.first?.enddatetime
                    let csvDate = ShareMethods.initDate(year, month, day, hour, min, sec)!
                    if enddatetime!.compare(csvDate) == .orderedDescending {
                        let cmpEnd = ShareMethods.initDateComponents(from: enddatetime!)
                        year = cmpEnd.year!
                        month = cmpEnd.month!
                        day = cmpEnd.day!
                    }
                } else if columnName == "temp_startdatetime" {
                    let startdatetime = routine[0].startdatetime
                    if routine.first?.temp_startdatetime != nil && (routine[0].temp_startdatetime.compare(routine[0].startdatetime) == .orderedAscending || routine[0].temp_startdatetime == routine[0].startdatetime) {
                        //すでにレコードがある場合
                        let cmpStart = ShareMethods.initDateComponents(from: startdatetime)
                        year = cmpStart.year!
                        month = cmpStart.month!
                        day = cmpStart.day!
                        hour = cmpStart.hour!
                        min = cmpStart.minute!
                        sec = cmpStart.second!
                    } else {
                        pRealmObject.setValue(startdatetime, forKey: "temp_startdatetime")
                    }
                }
            }
        }

        let dateVal = ShareMethods.initDate(year, month, day, hour, min, sec, nanosec)
        pRealmObject.setValue(dateVal, forKey: columnName)
        
    default:
        return
    }
}
    
//=========================================================================================================================================================================================================================================================================
}
