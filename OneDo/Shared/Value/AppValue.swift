//
//  SharedValue.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/16.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation

class AppValue {
    
    public class ViewSetting {
        static let NORTIFICATION_LINE_LENGTH_IPHONE7 : Int = 16
    }
    
    public class StoryBoardName {
        static let Main : String = "Main"
        static let RoutineGrp : String = "RoutineGrp"
    }
    
    public class TimeFormat {
        static let NOT_ZERO_ONLY_JPN = ""
    }
    
    public class DB {
        public class Migration {
            static let SCHEMA_VERSION: UInt64 = 13
            //  1 : 初期DB
            //  2 : od_t_routine + url
            //  3 : od_t_routine + temp_startdatetime
            //  4 : RealmObjectEx + PrimaryKey
            //  5 : od_t_task double_priority -> int_priority
            //  6 : + mm_m_event, od_t_routine_event
            //  7 : od_t_routine + events
            //  8 : od_t_routine + ignoredProperty
            //  9 : od_t_routine - ignoredProperty
            // 10 : od_t_routine - events
            // 11 : od_t_routine + bool_important
            // 12 : od_t_routine + bool_private
            // 13 : mm_m_user + bool_private
        }
        public class Table {
            public class mm_m_value {
                
                public class category {
                    static let span = "span"
                    static let timing = "timing"
                    static let routine_setting_nothing = "routine_setting_nothing"
                    static let routine_setting_home_outside = "routine_setting_home_outside"
                    static let routine_setting_move = "routine_setting_move"
                    static let routine_setting_workplace = "routine_setting_workplace"
                    static let routine_setting_health = "routine_setting_health"
                    static let routine_setting_mental = "routine_setting_mental"
                    static let routine_setting_diet = "routine_setting_diet"
                    static let routine_setting_breaktime = "routine_setting_breaktime"
                    static let routine_category_weather = "routine_category_weather"
                    static let system_setting = "system_setting"
                    static let weather = "weather"
                    static let place = "place"
                }
                
                public class span {
                    static let Year = "Y"
                    static let Month = "Mo"
                    static let Week = "W"
                    static let Day = "D"
                    static let Hour = "H"
                    static let Minute = "Mi"
                    static let Second = "S"
                    
                    static let Sun0 = "Su0"
                    static let Sun1 = "Su1"
                    static let Sun2 = "Su2"
                    static let Sun3 = "Su3"
                    static let Sun4 = "Su4"
                    static let Sun5 = "Su5"
                    
                    static let Mon0 = "Mo0"
                    static let Mon1 = "Mo1"
                    static let Mon2 = "Mo2"
                    static let Mon3 = "Mo3"
                    static let Mon4 = "Mo4"
                    static let Mon5 = "Mo5"
                    
                    static let Tue0 = "Tu0"
                    static let Tue1 = "Tu1"
                    static let Tue2 = "Tu2"
                    static let Tue3 = "Tu3"
                    static let Tue4 = "Tu4"
                    static let Tue5 = "Tu5"
                    
                    static let Wed0 = "We0"
                    static let Wed1 = "We1"
                    static let Wed2 = "We2"
                    static let Wed3 = "We3"
                    static let Wed4 = "We4"
                    static let Wed5 = "We5"
                    
                    static let Thu0 = "Th0"
                    static let Thu1 = "Th1"
                    static let Thu2 = "Th2"
                    static let Thu3 = "Th3"
                    static let Thu4 = "Th4"
                    static let Thu5 = "Th5"
                    
                    static let Fri0 = "Fr0"
                    static let Fri1 = "Fr1"
                    static let Fri2 = "Fr2"
                    static let Fri3 = "Fr3"
                    static let Fri4 = "Fr4"
                    static let Fri5 = "Fr5"
                    
                    static let Sat0 = "Sa0"
                    static let Sat1 = "Sa1"
                    static let Sat2 = "Sa2"
                    static let Sat3 = "Sa3"
                    static let Sat4 = "Sa4"
                    static let Sat5 = "Sa5"
                }
                
                public class holiday {
                    static let Anniversary = "A"
                    static let Sun = "Sun"
                    static let Mon = "Mon"
                    static let Tue = "Tue"
                    static let Wed = "Wed"
                    static let Thu = "Thu"
                    static let Fri = "Fri"
                    static let Sat = "Sat"
                }
                
                public class routine_setting {
                    static let nothing = "00"
                    static let home_only = "01"
                    static let outside_only = "02"
                    static let move_only = "01"
                    static let move_not = "02"
                    static let workplace_only = "01"
                    static let workplace_not = "02"
                    static let health_only = "01"
                    static let health_not = "02"
                    static let mental_only = "01"
                    static let mental_not = "02"
                    static let diet_only = "01"
                    static let diet_not = "02"
                    static let breaktime_only = "01"
                    static let breaktime_not = "02"
                    static let weather_sunny_only = "01"
                    static let weather_sunny_not = "02"
                    static let weather_rainy_only = "03"
                    static let weather_rainy_not = "04"
                    static let weather_cloudy_only = "05"
                    static let weather_cloudy_not = "06"
                    static let weekday_normal_only = "01"
                    static let weekday_holiday_only = "02"
                }
                
                public class system_setting {
                    static let other_routinegrp_name = "01"
                }
                
                public class weather {
                    static let sunny = "01"
                    static let rainy = "02"
                    static let cloudy = "03"
                }
                
                public class place {
                    static let home = "01"
                    static let outside = "02"
                    static let work = "03"
                }
                
                public class url {
                    static let web = "01"
                    static let application = "02"
                }
                
                public class event_trigger {
                    static let not_change = "00"
                    static let off = "01"
                    static let on = "02"
                }
            }
        }
    }
}

