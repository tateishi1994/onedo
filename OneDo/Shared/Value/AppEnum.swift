//
//  AppEnum.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/06/13.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation

enum ActionIdentifierRoutine : String {
    case START
    case FINISH
    case HOLD
    case FINISH_SUB
    case HOLD_SUB
    case SELECT_EVENT
    case URL
    
    var name: String {
        switch self {
        case ActionIdentifierRoutine.START:
            return "開始"
        case ActionIdentifierRoutine.FINISH:
            return "完了"
        case ActionIdentifierRoutine.HOLD:
            return "保留"
        case ActionIdentifierRoutine.FINISH_SUB:
            return "サブルーティン完了"
        case ActionIdentifierRoutine.HOLD_SUB:
            return "サブルーティン保留"
        case ActionIdentifierRoutine.SELECT_EVENT:
            return "イベント選択"
        case ActionIdentifierRoutine.URL:
            return "アプリを開く"
        }
    }
}
