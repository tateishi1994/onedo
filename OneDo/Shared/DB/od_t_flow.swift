//
//  od_t_flow.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/25.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class od_t_flow: RealmObjectEx {
    //フローID
    @objc dynamic var flowid: Int = 0
    //フロー名
    @objc dynamic var flowname: String = ""
    //並び順
    @objc dynamic var int_index: Int = 0

    //キー情報 ====================================================================================
    @objc override open class func primaryKey() -> String? { return "flowid" }
    
    override func incrementKey() -> [String] { return ["flowid", "int_index"] }
    
}
