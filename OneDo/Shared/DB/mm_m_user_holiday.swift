//
//  mm_m_user_holiday.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/07/04.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation

import RealmSwift

public class mm_m_user_holiday: RealmObjectEx {

    //ID
    @objc dynamic var id : Int = 0
    //ユーザーID
    @objc dynamic var userid : Int = 0
    //休日ID
    @objc dynamic var holidayid : Int = 0
    
    @objc override open class func primaryKey() -> String? { return "id" }
    
    override func incrementKey() -> [String] { return ["id"] }

}
