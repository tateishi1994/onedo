//
//  mm_m_cdname.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/31.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class mm_m_value: RealmObjectEx {
    
    @objc dynamic var id: Int = 0

    @objc dynamic var category: String = ""
    
    @objc dynamic var key: String = ""
    
    @objc dynamic var cd: String = ""
    
    @objc dynamic var str1: String = ""
    
    @objc dynamic var str2: String = ""
        
    @objc dynamic var int1: Int = 0
    
    @objc dynamic var int2: Int = 0
    
    @objc dynamic var bool1: Bool = false
    
    @objc override open class func primaryKey() -> String? { return "id" }
    
    override func incrementKey() -> [String] { return ["id"] }

}
