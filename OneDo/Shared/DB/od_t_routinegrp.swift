//
//  od_t_routinegrp.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/04/30.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class od_t_routinegrp: RealmObjectEx  {
    //ルーティングループID
    @objc dynamic var routine_grpid: Int = 0
    //ルーティングループ名
    @objc dynamic var routine_grpname: String = ""
    //並び順
    @objc dynamic var int_index: Int = 0
    //デフォルト優先度
    @objc dynamic var double_priority: Double = 0
    //フォルダーID
    @objc dynamic var folderid: Int = 0
    //フローID
    @objc dynamic var flowid: Int = 0
    
    //キー情報 ====================================================================================
    @objc override open class func primaryKey() -> String? { return "routine_grpid" }
    
    override func incrementKey() -> [String] { return ["routine_grpid", "int_index"] }
    
}
