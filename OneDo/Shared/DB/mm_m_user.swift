//
//  mm_m_user.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/04/30.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class mm_m_user: RealmObjectEx {
    
    //ユーザーID
    @objc dynamic var userid : Int = 0
    //メールアドレス
    @objc dynamic var mailaddress: String = ""
    //ユーザー名
    @objc dynamic var username: String = ""
    //起床時刻
    @objc dynamic var wakeuptime: Date? = Date()
    //就寝時刻
    @objc dynamic var bedtime: Date? = Date()
    //通知フラグ
    @objc dynamic var bool_alert: Bool = false
    //体調良好フラグ
    @objc dynamic var bool_health: Bool = false
    //食事規制中フラグ
    @objc dynamic var bool_diet: Bool = false
    //休憩中フラグ
    @objc dynamic var bool_breaktime: Bool = false
    //休暇フラグ
    @objc dynamic var bool_holiday: Bool = false
    //休暇フラグ
    @objc dynamic var bool_private: Bool = false
    //場所区分(在宅・外出中・職場)
    @objc dynamic var category_place: String = ""
    //天気
    @objc dynamic var category_weather: String = ""
    
    @objc override open class func primaryKey() -> String? { return "userid" }
    
    override func incrementKey() -> [String] { return ["userid"] }
}
