//
//  od_t_folder.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/01.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class od_t_folder: RealmObjectEx  {
    //フォルダID
    @objc dynamic var folderid : Int = 0
    //フォルダ名
    @objc dynamic var foldername : String = ""
    //並び順
    @objc dynamic var int_index: Int = 0
    
    //キー情報 ====================================================================================
    @objc override open class func primaryKey() -> String? { return "folderid" }
    
    override func incrementKey() -> [String] { return ["folderid", "int_index"] }
}
