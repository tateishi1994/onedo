//
//  mm_m_routinetag.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/31.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class mm_m_routinetag: RealmObjectEx {

    @objc dynamic var tagid: Int = 0
    
    @objc dynamic var tagname: String = ""
    
    @objc dynamic var int_priority: Int = 0
    
    @objc dynamic var color: String = ""
    
    @objc override open class func primaryKey() -> String? { return "tagid" }
}
