//
//  od_t_task.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/07/04.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class od_t_task: RealmObjectEx {
    //タスクID
    @objc dynamic var taskid: Int = 0
    //タスク名
    @objc dynamic var taskname: String = ""
    //ルーティンID
    @objc dynamic var routineid: Int = 0
    //優先度
    @objc dynamic var int_priority: Int = 0
    //チェックフラグ
    @objc dynamic var bool_checked: Bool = false

    //キー情報 ====================================================================================
    @objc override open class func primaryKey() -> String? { return "taskid" }
    
    override func incrementKey() -> [String] { return ["taskid"] }
    
}

