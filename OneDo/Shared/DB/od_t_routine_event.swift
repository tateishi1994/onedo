//
//  od_t_routine_event.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/07/12.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class od_t_routine_event: RealmObjectEx  {
    //ルーティンイベントID
    @objc dynamic var routine_eventid: Int = 0
    //ルーティンID
    @objc dynamic var routineid: Int = 0
    //イベントID
    @objc dynamic var eventid: Int = 0
    //ルーティン有効フラグ（イベント発生時にルーティンを行うかどうかを判別）
    @objc dynamic var bool_effective: Bool = false
    
//    let owners = LinkingObjects(fromType: od_t_routine.self, property: "events")
    
    //キー情報 ====================================================================================
    @objc override open class func primaryKey() -> String? { return "routine_eventid" }
    
    override func incrementKey() -> [String] { return ["routine_eventid"] }
    
}
