//
//  od_t_event.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/07/11.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class mm_m_event: RealmObjectEx {
    //イベントID
    @objc dynamic var eventid: Int = 0
    //イベント名
    @objc dynamic var eventname: String = ""
    //ユーザーID
    @objc dynamic var userid: Int = 0
    //イベントONフラグ
    @objc dynamic var bool_event_on: Bool = false
    //通知アクション表示フラグ
    @objc dynamic var bool_nortification_action: Bool = false
    //表示順
    @objc dynamic var int_index: Int = 0

    //キー情報 ====================================================================================
    @objc override open class func primaryKey() -> String? { return "eventid" }
    
    override func incrementKey() -> [String] { return ["eventid", "int_index"] }
    
}
