//
//  od_t_routine.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/04/29.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class od_t_routine: RealmObjectEx {
    
    //ルーティンID
    @objc dynamic var routineid: Int = 0
    //ルーティン名
    @objc dynamic var routinename: String = ""
    //サブタイトル
    @objc dynamic var subtitle: String = ""
    //前に行うルーティン
    @objc dynamic var double_priority: Double = 0
    //メモ
    @objc dynamic var memo: String = ""

    //▼ 外部キー ▼
    //ルーティングループID
    @objc dynamic var routine_grpid: Int = 0
    //前提ルーティンID
    @objc dynamic var pre_routineid: Int = 0

    //▼ ルーティン間隔関係 ▼
    //ルーティン開始日時
    @objc dynamic var startdatetime: Date = Date()
    //ルーティン消滅日時
    @objc dynamic var enddatetime: Date = Date()
    //一時ルーティン開始日時
    @objc dynamic var temp_startdatetime: Date = Date()
    //所要時間
    @objc dynamic var requiredtime: Date = Date()

    //ルーティン間隔
    @objc dynamic var int_span: Int = 1
    @objc dynamic var spanid: String = ""
    @objc dynamic var category_near_holiday: String = AppValue.DB.Table.mm_m_value.routine_setting.nothing
    
    //Webサイト・起動アプリURL
    @objc dynamic var routine_url: String = ""
    
    //締切時開始日リセットフラグ
    @objc dynamic var bool_reset_startdate: Bool = false
    //月曜フラグ
    @objc dynamic var bool_monday: Bool = false
    //火曜フラグ
    @objc dynamic var bool_tuesday: Bool = false
    //水曜フラグ
    @objc dynamic var bool_wednesday: Bool = false
    //木曜フラグ
    @objc dynamic var bool_thursday: Bool = false
    //金曜フラグ
    @objc dynamic var bool_friday: Bool = false
    //土曜フラグ
    @objc dynamic var bool_saturday: Bool = false
    //日曜フラグ
    @objc dynamic var bool_sunday: Bool = false
    //平日・休日区分
    @objc dynamic var category_weekday: String = AppValue.DB.Table.mm_m_value.routine_setting.nothing
    //天気区分
    @objc dynamic var category_weather: String = AppValue.DB.Table.mm_m_value.routine_setting.nothing

    //実行場所ID
    @objc dynamic var addressid: Int = 0
    //同時進行フラグ
    @objc dynamic var bool_simultaneous: Bool = false
    //重要ルーティンフラグ
    @objc dynamic var bool_important: Bool = false
    //プライベートフラグ
    @objc dynamic var bool_private: Bool = false
    
//    let events = List<od_t_routine_event>()
    
    //キー情報 ====================================================================================
    @objc override open class func primaryKey() -> String? { return "routineid" }
    
    override func incrementKey() -> [String] { return ["routineid"] }
    
//    override public static func ignoredProperties() -> [String] {
//        return ["events"]
//    }
}
