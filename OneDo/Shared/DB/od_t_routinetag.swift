//
//  od_t_routinetag.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/31.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class od_t_routinetag: RealmObjectEx {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var routineid: Int = 0
    @objc dynamic var tagid: Int = 0
    
    @objc override open class func primaryKey() -> String? { return "id" }
}
