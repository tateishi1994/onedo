//
//  AppDelegate.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/04/29.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private let programName = "AppDelegate"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    public func addNortification(_ request: UNNotificationRequest) {
        
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests(); // トリガーされている全ての通知をトリガー解除する
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in if granted { print("通知許可")}} // 「通知を許可しますか？」ダイアログを出す
        center.add(request)
        center.delegate = self
    }
    
    public func addNortification(_ center: UNUserNotificationCenter) {
        center.delegate = self
    }

}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // アプリ起動中でもアラートと音で通知
        completionHandler([.alert, .sound])
    }

//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        completionHandler()
//    }
    func applicationDidEnterBackground(_ application: UIApplication) {
//        let realm = try! Realm()
    }
    
    // アクションを選択した際に呼び出されるメソッド
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: () -> Swift.Void) {
        
        let realm = try! Realm()
        let userInfo = response.notification.request.content.userInfo
        let identifier = response.notification.request.content.categoryIdentifier
        let mailaddress = userInfo["mailaddress"] as? String
        let loginInfo = LoginInfo()
        loginInfo.login_userid = mailaddress!
        //        let user = realm.objects(mm_m_user.self).filter("mailaddress = %@", mailaddress!).first!
        switch true {
        case identifier.contains("routine_action"):
            
                    let routineidNow = userInfo[od_t_routine.primaryKey()!] as? Int
                    let routineNow = realm.objects(od_t_routine.self).filter("routineid = %@", routineidNow!).first!
                    
                    switch response.actionIdentifier {
                    case ActionIdentifierRoutine.FINISH.rawValue: //ルーティン完了
                        if routineidNow == 0 { return }
                        AppMethods.finishRoutine(realm, routineNow, self.programName, loginInfo)
                        AppMethods.addActiveRoutineNortification(realm, loginInfo) //次のルーティンを通知
                    case ActionIdentifierRoutine.FINISH_SUB.rawValue: //サブルーティン完了
                        let routineid_sub = userInfo["routineid_sub"] as? Int
                        if routineid_sub == 0 { return }
                        let routine_sub = realm.objects(od_t_routine.self).filter("routineid = %@", routineid_sub!).first!
                        AppMethods.finishRoutine(realm, routine_sub, self.programName, loginInfo)
                        AppMethods.addActiveRoutineNortification(realm, loginInfo) //次のルーティンを通知
                    case ActionIdentifierRoutine.HOLD.rawValue:
                        let inputResponse = response as? UNTextInputNotificationResponse
                        let inputText = inputResponse?.userText ?? ""
                        if let holdMinute = Int(inputText) {
                            //数値が入力された場合
                            AppMethods.holdRoutine(realm, routineNow, holdMinute, self.programName, loginInfo)
                        } else {
                            var holdMinute = 0
                            switch inputText {
                            case "明日", "あす", "あした", "Tomorrow", "Tom", "tomorrow", "tom":
                                holdMinute = 1400
                            default :
                                holdMinute = 0
                            }
                            AppMethods.holdRoutine(realm, routineNow, holdMinute, self.programName, loginInfo)
                        }
                        AppMethods.addActiveRoutineNortification(realm, loginInfo) //次のルーティンを通知
                    case ActionIdentifierRoutine.HOLD_SUB.rawValue:
                        let routineid_sub = userInfo["routineid_sub"] as? Int
                        if routineid_sub == 0 { return }
                        let routine_sub = realm.objects(od_t_routine.self).filter("routineid = %@", routineid_sub!).first!
                        let inputResponse = response as? UNTextInputNotificationResponse
                        let inputText = inputResponse?.userText ?? ""
                        if let holdMinute = Int(inputText) {
                            //数値が入力された場合
                            AppMethods.holdRoutine(realm, routine_sub, holdMinute, self.programName, loginInfo)
                        }
                        AppMethods.addActiveRoutineNortification(realm, loginInfo) //次のルーティンを通知
                    case ActionIdentifierRoutine.SELECT_EVENT.rawValue:
                        AppMethods.addSelectEventNortification(realm, loginInfo) //イベント選択通知
                    case ActionIdentifierRoutine.URL.rawValue: //外出
                        ShareMethods.openApplication(routineNow.routine_url)
                    default:
                        return
                    }
        default:
            
            let actionIdentifier = Int(response.actionIdentifier)
            if actionIdentifier == nil {
                AppMethods.addSelectEventNortification(realm, loginInfo) //イベント選択通知
            }
            AppMethods.switchEventState(realm, actionIdentifier!, programName, loginInfo)
            AppMethods.addActiveRoutineNortification(realm, loginInfo) //次のルーティンを通知
        }
        AppMethods.modifyRoutine(realm, self.programName, loginInfo) //テストデータ修正 !!!!!的的に開始終了を更新する機能を実装する必要がある
        completionHandler()
    }
    
}
