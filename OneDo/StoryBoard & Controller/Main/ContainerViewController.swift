//
//  ContainerViewController.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/04.
//  Copyright © 2020 at-works. All rights reserved.
//

import UIKit
import Foundation

class ContainerViewController : UIViewControllerEx {
    
    @IBOutlet weak var buttonPlus: UIButton!
    
//    private var coder : NSCoder
    
//    required init?(coder pDecoder: NSCoder) {
//        self.coder = pDecoder
//        super.init(coder : pDecoder)
//
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //+ボタンは丸くする
        self.buttonPlus.layer.cornerRadius = self.buttonPlus.layer.bounds.width / 2
    }
    
    @IBAction func BtnCreateRtn_TouchUpInside(_ sender: Any) {
        let storyboard : UIStoryboard = UIStoryboard(name : SharedValue.StoryBoardName.RoutineGrp, bundle : nil)
        let next = storyboard.instantiateInitialViewController()
//        let modalViewController = ModalSaveViewController(coder : self.coder)
//        modalViewController?.modalPresentationStyle = .custom
//        modalViewController?.UIViewControllerTransitioningDelegate = self
//        next?.modalPresentationStyle = .custom
        present(next!, animated : true, completion : nil)
    }
}

extension ContainerViewController: UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return ModalCustomViewController(presentedViewController: presented, presenting: presenting)
    }
}
