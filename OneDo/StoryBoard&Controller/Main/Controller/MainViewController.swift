//
//  TableViewController.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/04/30.
//  Copyright © 2020 at-works. All rights reserved.
//

import UIKit
import RealmSwift
import Foundation
//import UserNotifications

class MainViewController: UIViewControllerEx {
    
//== ▼ Outlet ▼ ====================================================================================================================
    
    @IBOutlet weak var tableView: UITableViewEx!
    @IBOutlet weak var btnReload: UIBarButtonItem!
    
//== ▼ Field ▼ ====================================================================================================================

    private let SECTION_HEIGHT : CGFloat = 50.0
    
    override var shouldAutorotate: Bool {
            //縦画面なので縦に固定
            UIDevice.current.setValue(1, forKey: "orientation")
            return false
    }
    
//== ▼ init ▼ ====================================================================================================================
    
    required init?(coder pDecoder: NSCoder) {
        super.init(coder: pDecoder)
        self.programname = "HOME"
        RealmMethods.realmMigration(AppValue.DB.Migration.SCHEMA_VERSION)
    }
    
//== ▼ Event ▼ ====================================================================================================================
// - ▼ View ▼ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //画像を半透明にして表示
//        let image = UIImage(named: "backGroundImage1")
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height))
//        imageView.image = image
//        imageView.alpha = 0.3 //透明度
        self.tableView.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 0.4, alpha:1.0)
//
//        self.tableView.backgroundView = imageView
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let realm = try! Realm()
        let user : mm_m_user = realm.objects(mm_m_user.self).filter("userid = %@", 1).first!
        self.loginInfo.login_userid = user.mailaddress
        self.loginInfo.bool_private = user.bool_private
//        AppMethods.upsertTestData(realm, self.programname, self.loginInfo) //テストデータ作成
//        AppMethods.modifyRoutine(realm, self.programname, self.loginInfo) //テストデータ修正
        self.createItems(realm) //フォルダ・ルーティングループのデータをセット
        AppMethods.addActiveRoutineNortification(realm, self.loginInfo) //最優先ルーティンを通知
    }
    
    @IBAction func Tapped(_ sender: Any) {
        let realm = try! Realm()
        AppMethods.modifyRoutine(realm, self.programname, self.loginInfo) //テストデータ修正 !!!!!的的に開始終了を更新する機能を実装する必要がある
        AppMethods.addActiveRoutineNortification(realm, self.loginInfo)
    }
    
//== ▼ Function( Initialize ) ▼ =====================================================================================================
    private func createItems(_ pRealm :Realm) {
        
        let folder = pRealm.objects(od_t_folder.self)
        let routinegrp = pRealm.objects(od_t_routinegrp.self)
        var index : Int = 0
        
        for rowFolder in folder {
            var items = [CellItem]()
            for rowRoutineGrp in routinegrp {
                if rowRoutineGrp.folderid == rowFolder.folderid {
                    let item = CellItem()
                    item.title = rowRoutineGrp.routine_grpname
                    item.id = rowRoutineGrp.routine_grpid
                    items.append(item)
                }
            }
            let sectionItem = SectionItem()
            sectionItem.sectionname = rowFolder.foldername
            sectionItem.cellItems = items
            sectionItem.index = index
            self.tableView.sections.append(sectionItem)
            index += 1
        }
        var items = [CellItem]()
        for row in routinegrp {
            if row.folderid == 0 {
                let item = CellItem()
                item.title = row.routine_grpname
                item.id = row.routine_grpid
                items.append(item)
            }
        }
        if items.count > 0 {
            let sectionItem = SectionItem()
            let otherRoutineGrpName = pRealm.objects(mm_m_value.self)
                                    .filter("category = %@ && key = %@", AppValue.DB.Table.mm_m_value.category.system_setting, AppValue.DB.Table.mm_m_value.system_setting.other_routinegrp_name)[0]
            sectionItem.sectionname = otherRoutineGrpName.str1
            sectionItem.cellItems = items
            sectionItem.index = index
            self.tableView.sections.append(sectionItem)
        }
    }
}
//== ▼ tableView関連 ▼ =====================================================================================================
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    // - ▼ TableView（ Section ） ▼ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        // セクション数
               func numberOfSections(in tableView: UITableView) -> Int {
                return self.tableView.sections.count
               }

               // セクションのタイトル
               func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
                   return self.tableView.sections[section].sectionname
               }

               // セクションの設定
               func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

                   let headerView = HeaderViewEx(tableView: self.tableView, section: section)
                   headerView.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 0.4, alpha:1.0)
                   let label = UILabel(frame: headerView.frame)
                   label.text = self.tableView.sections[section].sectionname
                   label.textColor = UIColor.black
                label.font = UIFont.boldSystemFont(ofSize: 17.0)
    //               let image = UIImage(named: "folder")
    //               let imageView = UIImageView(image: image)
    //               imageView.frame = CGRect(x:0, y:0, width:30, height:30)
    //               headerView.addSubview(imageView)
    //               cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
                   headerView.addSubview(label)
                   return headerView
               }

               func tableView(didSelectRowAt indexPath: IndexPath) {
                   tableView.deselectRow(at: indexPath, animated: true)
               }

               //セクションの高さ
               func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
                    return SECTION_HEIGHT
                }

        //// - ▼ TableView（ Cell ） ▼ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
            // セル数
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if self.tableView.sections[section].bool_open == true {
                return self.tableView.sections[section].cellItems.count
            }
            return 0
    //        return sections[section].bool_open ? sections[section].cellItems.count : 0
        }
            
            // セルの設定
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellRoutineGroup", for: indexPath)
            cell.textLabel?.text = self.tableView.sections[indexPath.section].cellItems[indexPath.row].title
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 16.5)
            cell.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width * 0.85, height: 20)
            cell.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 0.6, alpha:1.0)
        
    //        cell.backgroundColor = UIColor.clear //背景画像を見せるためにセルは半透明
    //        cell.contentView.backgroundColor = UIColor.clear //背景画像を見せるためにセルは半透明
            return cell
        }
}
//===================================================================================================================================
