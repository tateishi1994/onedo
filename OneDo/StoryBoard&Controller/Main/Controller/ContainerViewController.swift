//
//  ContainerViewController.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/04.
//  Copyright © 2020 at-works. All rights reserved.
//

import UIKit
import RealmSwift

class ContainerViewController : UIViewControllerEx {
    
    @IBOutlet weak var buttonPlus: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //+ボタンは丸くする
//        self.buttonPlus.layer.cornerRadius = self.buttonPlus.layer.bounds.width / 2
    }
    
    @IBAction func BtnCreateRtn_TouchUpInside(_ sender: Any) {
//        let storyboard : UIStoryboard = UIStoryboard(name : AppValue.StoryBoardName.RoutineGrp, bundle : nil)
//        let next = storyboard.instantiateInitialViewController()
//        present(next!, animated : true, completion : nil)
        self.programname = "HOME"
        self.loginInfo.login_userid = "tate-kita@i.softbank.jp"
        let realm = try! Realm()
        AppMethods.modifyRoutine(realm, self.programname, self.loginInfo) //テストデータ修正 !!!!!的的に開始終了を更新する機能を実装する必要がある
        AppMethods.addActiveRoutineNortification(realm, self.loginInfo)
    }
}

