//
//  RealmMethods.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/25.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

public class RealmMethods {
    
    class public func realmMigration(_ schemaVersion: UInt64) {
        // Realmマイグレーションバージョン
        // レコードフォーマットを変更する場合、このバージョンも上げていく。
        let migSchemaVersion: UInt64 = schemaVersion

        // マイグレーション設定
        let config = Realm.Configuration(
            schemaVersion: migSchemaVersion,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < migSchemaVersion) {
        }})
        Realm.Configuration.defaultConfiguration = config
    }
    
    class public func save<T : RealmObjectEx>(_ pRealm: Realm, _ pRealmObject : T, _ pDBProcess: DBProcess, _ pProgramName: String, _ pLoginInfo: LoginInfo) {
        
        var dbProcess = pDBProcess
        if pDBProcess == DBProcess.UPSERT {
            let data = pRealm.objects(T.self).filter(T.self.primaryKey()! + " = %@", pRealmObject.value(forKey: T.self.primaryKey()!)!)
            if data.count > 0 {
                dbProcess = DBProcess.UPDATE
            } else {
                dbProcess = DBProcess.INSERT
            }
        }
        
        pRealmObject.common = RealmMethods.setCommonInfo(pRealmObject.common, dbProcess, pProgramName, pLoginInfo)
        
        switch dbProcess {
        case DBProcess.INSERT:
            //新規登録時は自動採番してINSERTする
            for columnName in pRealmObject.incrementKey() {
                if pRealmObject.value(forKey: columnName) as? Int != 0 {
                    continue
                }
                let incremantVal = RealmMethods.getIncrementValue(pRealm, pRealmObject, columnName)
                pRealmObject.setValue(incremantVal, forKey: columnName)
            }
            pRealm.add(pRealmObject)
        case DBProcess.UPDATE:
            pRealm.add(pRealmObject, update: .modified)
            return
        case DBProcess.DELETE:
            pRealm.delete(pRealmObject)
            return
        default:
            return
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
    }
    
    class public func setCommonInfo(_ pCommonInfo : common_info?, _ pDBProcess : DBProcess, _ pProgramName : String, _ pLoginInfo : LoginInfo) -> common_info {
        
        var common = pCommonInfo
        
        if common == nil {
            common = common_info()
        }
        
        common?.updat = Date()
        common?.updpgm = pProgramName
        common?.updby = pLoginInfo.login_userid

        switch pDBProcess {
        case DBProcess.INSERT:
            common?.crtat = Date()
            common?.crtpgm = pProgramName
            common?.crtby = pLoginInfo.login_userid
            return common!
        case DBProcess.UPDATE, DBProcess.DELETE:
            return common!
        default:
            return common!
        }
    }
    
    class public func getIncrementValue<T: RealmSwift.Object>(_ pRealm : Realm,  _ pRealmObject : T, _ pColumnName : String) -> Int {
        let maxVal = (pRealm.objects(T.self).sorted(byKeyPath: pColumnName, ascending: false).first?.value(forKey: pColumnName) as? Int ?? 0) + 1
        return maxVal
    }

    class public func importCSV<T: RealmObjectEx>(_ pRealm : Realm, _ filename: String, _ pRealmObject: T, _ pProgramName : String, _ pLoginInfo : LoginInfo) {
        importCSV(pRealm, filename, pRealmObject, pProgramName, pLoginInfo, setValue)
    }
    
    class public func importCSV<T: RealmObjectEx>(_ pRealm : Realm, _ filename: String, _ pRealmObject: T, _ pProgramName : String, _ pLoginInfo : LoginInfo, _ pFuncSetVal: (Realm, T, String, String, String) -> ()) {
        //引数にインスタンスじゃなくて型だけを渡すことはできない？
        let csvData = ShareMethods.loadCSV(filename: filename)
        
        //CSVデータにないレコードはDELETEする
        let primaryKey = T.primaryKey()!
        let routines = pRealm.objects(T.self).sorted(byKeyPath: primaryKey, ascending: false)
        for row in routines {
            let id = row.value(forKey: primaryKey) as? Int
            var flgNotExist = true
            var keyColumnIndex : Int = 0
            for csvRow: [String] in csvData {
                var cnt : Int = 0
                for value in csvRow {
                    if value == T.primaryKey()! {
                        keyColumnIndex = cnt
                    }
                    cnt += 1
                }
                if csvRow[keyColumnIndex] == String(id!) {
                    flgNotExist = false
                    break
                }
            }
            if flgNotExist == true && String(id!) != "0" && id != nil {
                RealmMethods.save(pRealm, row, DBProcess.DELETE, pProgramName, pLoginInfo)
            }
        }
        
        var columnNames = [String]()
        var columnTypes = [String]()
        var rowIndex = 0
        
        for row in csvData {
            
            let realmObject = T()
            
            if rowIndex == 0 {
                //先頭行のカラム名を読み取る
                for value in row {
                    columnNames.append(value)
                }
            } else if rowIndex == 1 {
                //2行目のカラム型を読み取る
                for value in row {
                    columnTypes.append(value)
                }
            } else {
                //3行目以降
                var columnIndex = 0
                for value in row {
                    pFuncSetVal(pRealm, realmObject, columnNames[columnIndex], columnTypes[columnIndex], value)
                    columnIndex += 1
                }
//                if T.self == od_t_routine.self {
//                    let id = realmObject.value(forKey: "double_priority") as! Int
//                }

                let dbData = pRealm.objects(T.self).filter(T.self.primaryKey()! + " == %@", realmObject.value(forKey: T.self.primaryKey()!)!)
                if dbData.count > 0 {
                    RealmMethods.save(pRealm, realmObject, DBProcess.UPDATE, pProgramName, pLoginInfo)
                } else {
                    RealmMethods.save(pRealm, realmObject, DBProcess.INSERT, pProgramName, pLoginInfo)
                }
                
            }
            rowIndex += 1
        }
    }
    
    class private func setValue<T: RealmObjectEx>(_ pRealm: Realm, _ pRealmObject: T, _ columnName: String, _ columnType: String, _ value: String) {
        
        if value == "" {
            return
        }
        
        switch columnType {
        case "String":
            pRealmObject.setValue(value, forKey: columnName)
        case "Int":
            pRealmObject.setValue(Int(value), forKey: columnName)
        case "Double":
            pRealmObject.setValue(Double(value), forKey: columnName)
        case "Bool":
            if value.lowercased() == "true" {
                pRealmObject.setValue(true, forKey: columnName)
            } else if value.lowercased() == "false" {
                pRealmObject.setValue(false, forKey: columnName)
            }
        case "Date":
            //DateFormatterがうまくいかなかった
            let separatedBySpace = value.components(separatedBy: " ")
            var separatedBySlash : [String]
            var separatedByColon : [String]
            
            if separatedBySpace.count == 2 {
                let datePart = separatedBySpace[0]
                let timePart = separatedBySpace[1]
                separatedBySlash = datePart.components(separatedBy: "/")
                separatedByColon = timePart.components(separatedBy: ":")
            } else {
                separatedByColon = value.components(separatedBy: ":")
                separatedBySlash = value.components(separatedBy: "/")
            }
            
            var year : Int = 0
            var month : Int = 0
            var day : Int = 0
            var hour : Int = 0
            var min : Int = 0
            var sec : Int = 0
            var nanosec : Int = 0
            
            if separatedBySlash.count == 3 {
                year = Int(separatedBySlash[0])!
                month = Int(separatedBySlash[1])!
                day = Int(separatedBySlash[2])!
            } else {
                year = 1
                month = 1
                day = 1
            }
            
            if separatedByColon.count == 3 {
                 hour = Int(separatedByColon[0])!
                 min = Int(separatedByColon[1])!
                 let separatedByPeriod = separatedByColon[2].components(separatedBy: ".")
                if separatedByPeriod.count == 2 {
                    sec = Int(separatedByPeriod[0])!
                    nanosec = Int(separatedByPeriod[1])!
                } else {
                    sec = Int(separatedByColon[2])!
                    nanosec = 0
                }
            }
            
            let dateVal = ShareMethods.initDate(year, month, day, hour, min, sec, nanosec)
            pRealmObject.setValue(dateVal, forKey: columnName)
            
        default:
            return
        }
    }
    
    public class func deleteObjectData<T: RealmObjectEx>(_ pRealm: Realm, _ pRealmObject: T) {
        let allData = pRealm.objects(T.self)
        pRealm.delete(allData)
    }
}
