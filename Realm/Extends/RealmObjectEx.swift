//
//  RealmObjectEx.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/26.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation
import RealmSwift

public class RealmObjectEx : RealmSwift.Object, RealmObjectDelegate {
    
    required init() {
        
    }
    
    //主キー
    @objc final dynamic var primarykey: String = ""
    //▼ 共通列 ▼
    @objc dynamic var common: common_info? = common_info()
    
    @objc override open class func primaryKey() -> String? { return nil }
    
//    @objc open class func multiKey() -> [String]? { return nil }
    
    func incrementKey() -> [String] {
        //継承先に依存
        let arrayIncrementColumn : [String] = [String]()
        return arrayIncrementColumn
    }
}
