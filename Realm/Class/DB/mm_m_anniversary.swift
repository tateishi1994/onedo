//
//  mm_m_anniversary.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/06/14.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation
import RealmSwift

public class mm_m_anniversary: RealmObjectEx {
    //ID
    @objc dynamic var anniversaryid: Int = 0
    //祝祭日区分
    @objc dynamic var category: String = ""
    //祝祭日名
    @objc dynamic var name: String = ""
    //月
    @objc dynamic var month: Int = 0
    //日
    @objc dynamic var day: Int = 0
    //施行年
    @objc dynamic var year_start: Int = 0
    //廃止年
    @objc dynamic var year_end: Int = 0
    //キー情報 ====================================================================================
    //主キー
    @objc override open class func primaryKey() -> String? { return "anniversaryid" }
    //インクリメントキー
    override func incrementKey() -> [String] { return ["anniversaryid"] }
}
