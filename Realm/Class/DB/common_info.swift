//
//  _common_info.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/04/30.
//  Copyright © 2020 at-works. All rights reserved.
//

import RealmSwift

///テーブル共通カラム
public class common_info: Object {
    ///登録日
    @objc dynamic var crtat: Date = Date()
    ///登録者
    @objc dynamic var crtby: String = ""
    ///登録プログラム
    @objc dynamic var crtpgm: String = ""
    ///更新日
    @objc dynamic var updat: Date = Date()
    ///更新者
    @objc dynamic var updby: String = ""
    ///更新プログラム
    @objc dynamic var updpgm: String = ""
}
