//
//  RealmObjectDelegate.swift
//  OneDo
//
//  Created by 立石敦宏 on 2020/05/26.
//  Copyright © 2020 at-works. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmObjectDelegate {

//    func save(_ pRealm : Realm, _ pRealmObject : RealmSwift.Object, _ pDBProcess : DBProcess, _ pProgramName : String, _ pLoginInfo : LoginInfo)
    
    func incrementKey() -> [String]

}
